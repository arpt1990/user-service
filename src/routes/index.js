import {checkMobileNumber} from '../controllers/user';

const routes = app => {
  app.get('/user/check', checkMobileNumber);
};

export default routes;