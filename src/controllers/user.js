import {user as User} from "../models";
import url from 'url';

export const checkMobileNumber = (req, res) => {
  const mobile = url.parse(req.url, true).query.mobile;
  User
    .count({where: {mobile}})
    .then(count => {
      if (count) {
        return res.status(200).send();
      }
      res.status(404).send();
    });
};