**PLUGO USER SERVICE**

1. Database `plugo` must be created manually.
2. Run `yarn install` to install required packages.
3. Change the database configurations in `./config/config.json`.
4. Run `yarn start` to start the server on port `8080`.
5. Database `plugo_test` must be created manually for testing.
6. Run `yarn test` to run the test cases.
7. Run `yarn clean` to drop and re-create `plugo` database assuming that the database exists.
8. Run `yarn sqlcli` to invoke the `sequelize` command to preform sequelize-cli actions.
9. Run `yarn sqlcli db:migrate` to perform migrations.
10. Run `yarn sqlcli db:seed:all` while running `yarn start to seed sample data to connected database.
11. Run `yarn sqlcli db:seed:undo:all` to undo all seed operations.