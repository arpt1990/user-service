import request from 'supertest';
import app from '../app';
import db from '../src/models';
import {user as User} from "../src/models";

describe('GET /user/check', () => {

  beforeEach(done => {
    seedData(done);
  });

  afterEach(done => {
    db.sequelize
      .sync({force: true})
      .then(() => done());
  });

  it('should respond with 200 if number is present', done => {
    request(app)
      .get('/user/check')
      .query({mobile: '9900829903'})
      .expect(200, done);
  });

  it('should respond with 404 if number is not present', done => {
    request(app)
      .get('/user/check')
      .query({mobile: '9900829930'})
      .expect(404, done);
  });

});

const seedData = done => {
  db.sequelize
    .sync({force: true})
    .then(() => {
      const newUser = {
        mobile: '9900829903',
        firstname: 'Nikshep',
        lastname: 'A V',
        password: 'password',
      };
      User
        .create(newUser)
        .then(() => done())
        .catch(() => console.log('error'));
    });

};