import http from 'http';
import app from '../app';
import db from '../src/models';

const port = parseInt(process.env.PORT, 10) || 8080;

app.set('port', port);

const server = http.createServer(app);

db.sequelize
  .sync()
  .then(() => {
    server.listen(port);
  })
  .catch(() => console.log('Error syncing db'));
